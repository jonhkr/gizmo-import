require "bundler/gem_tasks"
require "survey-gizmo-ruby"
require "mysql2"
require "pry"
require "sanitize"


def save_or_update_surveys(surveys)
	by_id_stmt = @read_db.prepare("select * from survey where id = ?")
	insert_stmt = @write_db.prepare("insert into survey(subtype, status, title, statistics, created_at, modified_at, id) values (?,?,?,?,?,?,?)")
	update_stmt = @write_db.prepare("update survey set subtype = ?, status = ?, title = ?, statistics = ?, created_at = ?, modified_at = ? where id = ?")

	surveys.each do |survey|
		persisted = by_id_stmt.execute(survey.id)

		stmt = update_stmt

		if persisted.count == 0
			stmt = insert_stmt
		end

		stmt.execute(
			survey._subtype,
			survey.status,
			survey.title.clean,
			survey.statistics.to_s,
			survey.created_on.to_db_time,
			survey.modified_on.to_db_time,
			survey.id
		)

		check_api_query_count
		puts "Saving survey campaigns of survey id: #{survey.id}"
		save_or_update_survey_campaigns(survey)

		check_api_query_count
		puts "Saving pages of survey id: #{survey.id}"
		save_or_update_pages(survey.pages)

		resultsperpage = 500
		page = 1
		responses = nil
		while responses.nil? or responses.size == resultsperpage

			check_api_query_count
			puts "Fetching responses of survey id: #{survey.id}, page: #{page}"
			responses = SurveyGizmo::API::Response.all({ survey_id: survey.id }, { resultsperpage: resultsperpage, page: page })

			puts "#{responses.size} responses found, saving..."
			save_or_update_responses(responses)

			page += 1
		end
	end
end

def save_or_update_survey_campaigns(survey)
	by_id_stmt = @read_db.prepare("select * from survey_campaign where id = ?")
	insert_stmt = @write_db.prepare("insert into survey_campaign(name, _type, _subtype, __subtype, status, uri, language, date_created, date_modified, survey_id, id) values (?,?,?,?,?,?,?,?,?,?,?)")
	update_stmt = @write_db.prepare("update survey_campaign set name = ?, _type = ?, _subtype = ?, __subtype = ?, status = ?, uri = ?, language = ?, date_created = ?, date_modified = ?, survey_id = ? where id = ?")

	begin
		survey_campaigns = SurveyGizmo::API::SurveyCampaign.all(:survey_id => survey.id)
	rescue NoMethodError
		# Check this PR for an explanation for this:
		# https://github.com/RipTheJacker/survey-gizmo-ruby/pull/41
		survey_campaigns = []
	end
	survey_campaigns.each do |e|
		persisted = by_id_stmt.execute(e.id)

		stmt = update_stmt

		if persisted.count == 0
			stmt = insert_stmt
		end

		stmt.execute(
			e.name,
			e._type,
			e._subtype,
			e.__subtype,
			e.status,
			e.uri,
			e.language,
			e.datecreated.to_db_time,
			e.datemodified.to_db_time,
			e.survey_id,
			e.id
		)
	end
end

def save_or_update_pages(pages)
	by_id_stmt = @read_db.prepare("select * from page where id = ?")
	insert_stmt = @write_db.prepare("insert into page(title, description, survey_id, id) values (?,?,?,?)")
	update_stmt = @write_db.prepare("update page set title = ?, description = ?, survey_id = ? where id = ?")

	pages.each do |e|
		persisted = by_id_stmt.execute(e.id)
		stmt = update_stmt

		if persisted.count == 0
			stmt = insert_stmt
		end

		stmt.execute(
			e.title.clean,
			e.description.to_s,
			e.survey_id,
			e.id
		)

		check_api_query_count
		puts "Fetching questions of survey id: #{e.survey_id} and page id: #{e.id}"
		questions = SurveyGizmo::API::Question.all(survey_id: e.survey_id, page_id: e.id)

		puts "#{questions.size} questions found, saving..."
		save_or_update_questions(questions)
	end
end

def save_or_update_questions(questions)
	by_id_stmt = @read_db.prepare("select * from question where id = ?")
	insert_stmt = @write_db.prepare("insert into question(title, type, description, survey_id, page_id, parent_question_id, id) values (?,?,?,?,?,?,?)")
	update_stmt = @write_db.prepare("update question set title = ?, type = ?, description = ?, survey_id = ?, page_id = ?, parent_question_id = ? where id = ?")

	questions.each do |e|
		persisted = by_id_stmt.execute(e.id)
		stmt = update_stmt

		if persisted.count == 0
			stmt = insert_stmt
		end

		stmt.execute(
			e.title.clean,
			e.type.clean,
			e.description.to_s,
			e.survey_id,
			e.page_id,
			e.parent_question_id,
			e.id
		)

		check_api_query_count
		puts "Saving options of question id: #{e.id}"
		save_or_update_options(e.options)
	end
end

def save_or_update_options(options)
	by_id_stmt = @read_db.prepare("select * from `option` where id = ?")
	insert_stmt = @write_db.prepare("insert into `option`(survey_id, page_id, question_id, title, value, id) values (?,?,?,?,?,?)")
	update_stmt = @write_db.prepare("update `option` set survey_id = ?, page_id = ?, question_id = ?, title = ?, value = ? where id = ?")

	options.each do |e|
		persisted = by_id_stmt.execute(e.id)
		stmt = update_stmt

		if persisted.count == 0
			stmt = insert_stmt
		end

		stmt.execute(
			e.survey_id,
			e.page_id,
			e.question_id,
			e.title.clean,
			e.value.clean,
			e.id
		)
	end
end

def save_or_update_responses(responses)
	by_id_stmt = @read_db.prepare("select * from `response` where id = ?")
	insert_stmt = @write_db.prepare("insert into `response`(survey_id, status, datesubmitted, is_test_data, id) values (?,?,?,?,?)")
	update_stmt = @write_db.prepare("update `response` set survey_id = ?, status = ?, datesubmitted = ?, is_test_data = ? where id = ?")

	responses.each do |e|
		persisted = by_id_stmt.execute(e.id)
		stmt = update_stmt

		if persisted.count == 0
			stmt = insert_stmt
		end

		stmt.execute(
			e.survey_id,
			e.status,
			e.datesubmitted.to_db_time,
			e.is_test_data,
			e.id
		)

		puts "Saving meta information of response id: #{e.id}"
		save_or_update_response_meta(e)
		puts "Saving answers of response id: #{e.id}"
		save_or_update_response_answers(e)
	end
end

def save_or_update_response_meta(response)
	by_id_stmt = @read_db.prepare("select * from `response_meta` where response_id = ?")
	insert_stmt = @write_db.prepare("insert into `response_meta`(ip, `long`, lat, geocountry, geocity, georegion, responsetime, device, avgquestseconds, geodma, referer, useragent, response_id) values (?,?,?,?,?,?,?,?,?,?,?,?,?)")
	update_stmt = @write_db.prepare("update `response_meta` set ip = ?, `long` = ?, lat = ?, geocountry = ?, geocity = ?, georegion = ?, responsetime = ?, device = ?, avgquestseconds = ?, geodma = ?, referer = ?, useragent = ? where response_id = ?")

	persisted = by_id_stmt.execute(response.id)
	stmt = update_stmt

	if persisted.count == 0
		stmt = insert_stmt
	end

	stmt.execute(
		response.meta[:ip],
		response.meta[:long].to_f,
		response.meta[:lat].to_f,
		response.meta[:geocountry],
		response.meta[:geocity],
		response.meta[:georegion],
		response.meta[:responsetime].to_f,
		response.meta[:device],
		response.meta[:avgquestseconds].to_f,
		response.meta[:geodma],
		response.meta[:referer],
		response.meta[:useragent],
		response.id
	)
end

def save_or_update_response_answers(response)
	by_id_stmt = @read_db.prepare("select * from `response_answer` where response_id = ? and question_id = ?")
	insert_stmt = @write_db.prepare("insert into `response_answer`(answer, response_id, question_id) values (?,?,?)")
	update_stmt = @write_db.prepare("update `response_answer` set answer = ? where response_id = ? and question_id = ?")

	answers = response.answers.inject({}){ |hash, (k, v)| hash.merge( k.split(',')[0].scan(/\d+/).join('').to_i => v ) }

	answers.each do |question_id, answer|
		persisted = by_id_stmt.execute(response.id, question_id)
		stmt = update_stmt

		if persisted.count == 0
			stmt = insert_stmt
		end

		begin
			stmt.execute(
				answer,
				response.id,
				question_id
			)

		rescue Mysql2::Error => e
			puts e, question_id, answer
		end

		GC.start
	end
end

task :run, :user, :password, :db_host, :db_username, :db_password, :db_database do |t, args|
	SurveyGizmo.configure do |config|
	  config.user = args[:user]
	  config.password = args[:password]
	  config.api_version = 'v4'
	end

	@api_query_count = 0

	@read_db = Mysql2::Client.new(
		:host => args[:db_host],
		:username => args[:db_username],
		:password => args[:db_password],
		:database => args[:db_database]
	)

	@write_db = Mysql2::Client.new(
		:host => args[:db_host],
		:username => args[:db_username],
		:password => args[:db_password],
		:database => args[:db_database],
		:encoding => "utf8mb4"
	)

	check_api_query_count
	puts "Fetching surveys"
	surveys = SurveyGizmo::API::Survey.all

	puts "#{surveys.count} surveys found, saving..."
	save_or_update_surveys(surveys)

end

def check_api_query_count
	if @api_query_count == 10
		puts "API call limit exceded, sleeping for 1 minute"
		sleep(1.minute)
		@api_query_count = 1
	else
		@api_query_count += 1
	end
end


class String
  def to_db_time
    return if self.empty?
    DateTime.iso8601(self).to_time.utc.strftime("%Y-%m-%d %H:%M:%S")
  end

  def clean
  	Sanitize.fragment(self)
  end
end

class DateTime
  def to_db_time
    self.utc.strftime('%Y-%m-%d %H:%m:%S')
  end
end

class Time
	def to_db_time
		self.utc.strftime('%Y-%m-%d %H:%m:%S')
	end
end