
create table survey (
	id int,
	subtype varchar(255),
	status varchar(255),
	title text,
	statistics text,
	created_at datetime,
	modified_at datetime,
	PRIMARY KEY (id)
);

create table survey_campaign (
	id int,
	name varchar(255),
	_type varchar(255),
	_subtype varchar(255),
	__subtype varchar(255),
	status varchar(50),
	uri text,
	language varchar(50),
	survey_id int,
	date_created datetime,
	date_modified datetime,
	PRIMARY KEY (id),
	FOREIGN KEY (survey_id) REFERENCES survey(id)
);

create table page (
	id int,
	title text,
	description text,
	survey_id int,
	PRIMARY KEY (id),
	FOREIGN KEY (survey_id) REFERENCES survey(id)
);

create table question (
	id int,
	title text,
	type text,
	description text,
	survey_id int,
	page_id int,
	parent_question_id int,
	PRIMARY KEY (id),
	FOREIGN KEY (survey_id) REFERENCES survey(id),
	FOREIGN KEY (page_id) REFERENCES page(id)
);

create table `option` (
	id int,
	survey_id int,
	page_id int,
	question_id int,
	title text,
	value varchar(255),
	PRIMARY KEY (id),
	FOREIGN KEY (survey_id) REFERENCES survey(id),
	FOREIGN KEY (page_id) REFERENCES page(id),
	FOREIGN KEY (question_id) REFERENCES question(id)
);

create table response (
	id int,
	survey_id int,
	status varchar(255),
	datesubmitted datetime,
	is_test_data boolean,
	PRIMARY KEY (id),
	FOREIGN KEY (survey_id) REFERENCES survey(id)
);

create table response_meta (
	response_id int,
	ip varchar(255),
	`long` double,
	lat double,
	geocountry varchar(255),
	geocity varchar(255),
	georegion varchar(255),
	responsetime double,
	device varchar(255),
	avgquestseconds double,
	geodma varchar(255),
	referer text,
	useragent varchar(255),
	FOREIGN KEY (response_id) REFERENCES response(id)
);

create table response_answer (
	response_id int,
	question_id int,
	answer text,
	FOREIGN KEY (response_id) REFERENCES response(id),
	FOREIGN KEY (question_id) REFERENCES question(id)
);

ALTER TABLE response_answer MODIFY COLUMN answer text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;